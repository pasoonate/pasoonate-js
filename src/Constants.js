const Constants = {
	J1970: 2440587.5, // Julian date at Unix epoch: 1970-01-01
	SATURDAY: 1,
	Sunday: 2,
	Monday: 3,
	Tuesday: 4,
	Wednesday: 5,
	Thursday: 6,
	Friday: 7,
	YearsPerCentury: 100,
	YearsPerDecade: 10,
	MonthsPerYear: 12,
	WeeksPerYear: 52,
	DaysPerWeek: 7,
	HoursPerDay: 24,
	MinutesPerHour: 60,
	SecondsPerMinute: 60,
	DayInSecond: 86400,
	ShiaEpoch: 1948439.5,
	JalaliEpoch: 1948320.5,
	GregorianEpoch: 1721425.5,
	IslamicEpoch: 1948439.5,
	DaysOfIslamicYear: 354,
	DaysOfShiaYear: 354,
	DaysOfJalaliYear: 365,
	DaysOfGregorianYear: 365,
	Gregorian: 'gregorian',
	Jalali: 'jalali',
	Shia: 'shia',
	Islamic: 'islamic'
};

export default Constants;